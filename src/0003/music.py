import mido
import rtmidi
from mido import Message, MidiFile, MidiTrack
from time import sleep
import numpy as np
import sys
import vlc
import os
import subprocess
from pathlib import Path
from pygame import mixer
import simpleaudio as sa


# https://mido.readthedocs.io/en/latest/midi_files.html#opening-a-file

LOOKUP = {
        'Db': 'C#', 'D#': 'Eb', 'E#': 'F', 'Gb': 'F#',
        'G#': 'Ab', 'A#': 'Bb', 'B#': 'C', 'C': 'C',
        'C#': 'C#', 'D': 'D', 'Eb': 'Eb', 'E': 'E',
        'F': 'F', 'F#': 'F#', 'G': 'G', 'Ab': 'Ab',
        'A': 'A', 'Bb': 'Bb', 'B': 'B', 'Hb': 'Bb', 'H': 'B'
}
NOTES = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'Ab', 'A', 'Bb', 'B']
NOTES_IN_OCTAVE = len(NOTES)


def note_to_number(note: str, octave: int) -> int:
    assert note in LOOKUP, f'Unknown note: {note}'
    note = LOOKUP[note]
    assert octave > 0 and octave < 11, f'Octave out or range: {octave}'

    note = NOTES.index(note)
    note += (NOTES_IN_OCTAVE * octave)
    assert 0 <= note <= 127, f'Note out of range {note}'
    return note


    octave = 7
    bank_instrument = {
            0: [4, 19, 57],
            1: [8, 14],
            4: [7, 16, 27, 43],
            5: [7, ],
            23: [4, 64, 94],
            }

    octave = 3
    bank_instrument = {
            5: [7, 15, 31, 65, 75, ],
            18: [26, 27, 52, ],
            13: [7, 17, 37, ],
            23: [37],
            }


def get_mid(programs):
    mid = MidiFile(type=1)
    for i, p in enumerate(programs):
        mid.tracks.append(MidiTrack())
        mid.tracks[-1].append(Message('program_change', program=p, time=0, channel=0))
    return mid


def get_song(programs, n):
    first_octave = 2
    offset_factor = 2
    offsets = first_octave + np.arange(len(programs)) * offset_factor
    mask = programs >= 0
    programs = programs[mask]
    offsets = offsets[mask]

    mid = get_mid(programs)

    tempo = 500000  # microseconds per beat
    tpb = mid.ticks_per_beat

    duration = int(mido.second2tick(0.2, tpb, tempo))
    gap = int(mido.second2tick(0.01, tpb, tempo))

    keys = ["C", "D", "E", "F", "G", "A", "B"]

    for i in range(n):
        for c, track in enumerate(mid.tracks):
            note = note_to_number(keys[i%len(keys)], i//len(keys) + offsets[c])
            track.append(Message('note_on', note=note, channel=0, velocity=100, time=gap))
            track.append(Message('note_off', note=note, channel=0, velocity=127, time=duration))
    return mid


def mid2wav(mid, filename, sf="sf/Yamaha_YPT_220_soundfont_studio_version.sf2", wait=False):
    mid_pth = str(Path(filename).with_suffix(".mid"))
    out_pth = str(Path(filename).with_suffix(".wav"))
    mid.save(mid_pth)
    command = f"fluidsynth {sf} {mid_pth} -F {out_pth}"
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    if wait:
        process.wait()



def play_mid(filename, sf="sf/Yamaha_YPT_220_soundfont_studio_version.sf2"):
    pth = str(Path(filename).with_suffix(".mid"))
    command = f"fluidsynth {sf} {pth} -in"
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    sleep(2)
    # process.wait()


def play_wav(filename, sf):
    pth = str(Path(filename).with_suffix(".wav"))
    wave_obj = sa.WaveObject.from_wave_file(pth)
    play_obj = wave_obj.play()
    play_obj.wait_done()


def play_mp3_(pth):
    p = None
    try:
        p = vlc.MediaPlayer(pth)
        p.play()
        sleep(1.5)
        duration = p.get_length() / 1000
        sleep(duration)
    except KeyboardInterrupt:
        if p is not None:
            p.stop()
            raise KeyboardInterrupt()


def play_with_vlc(mid, sf="sf/Yamaha_YPT_220_soundfont_studio_version.sf2"):
    filename = Path("tmp")
    mid_pth = str(filename.with_suffix(".mid"))
    out_pth = str(filename.with_suffix(".mp3"))
    mid2mp3(mid, filename, sf, wait=True)
    play_mp3(out_pth)


def main(out):
    programs = np.array(sys.argv[1:], dtype=np.int32)
    objects = []

    upper = [108, 107, 80, 78, 72, 46, 39, 38, 37, 26, 15, 12, 11, 10, 8, 2] # 8, 26
    lower = [99, 92, 73, 72, 50] # 50
    sf = "sf/SGM-v2.01-HQ-v3.0.sf2"

    # vysky,
    filename = Path(f"tmp/current")
    wav_out = str(Path(filename).with_suffix(".wav"))
    mid_out = str(Path(filename).with_suffix(".mid"))

    upper = [46, 38, 26, 12, 11, 8, 2]
    lower = [72]
    programs = np.array([26]) #, 72])
    mid = get_song(programs, 30)
    mid.save(mid_out)
    play_mid(filename, sf=sf)
    # mid2wav(mid, filename, sf=sf, wait=True)


if __name__ == "__main__":
    midi_out = mido.open_output()
    try:
        main(midi_out)
        # main_file(midi_out)
        # main_load(midi_out)
    except KeyboardInterrupt:
        midi_out.reset()
