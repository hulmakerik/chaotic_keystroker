import pygame
import sys
import pygame._sdl2.audio as sdl2_audio
from typing import Tuple, Optional
from time import sleep
import pygame.midi
from mingus.core import chords
import numpy as np

# https://stackoverflow.com/questions/49543366/why-pygame-midi-doesnt-work-on-macos-whereas-pygame-mixer-does

LOOKUP = {
        'Db': 'C#', 'D#': 'Eb', 'E#': 'F', 'Gb': 'F#',
        'G#': 'Ab', 'A#': 'Bb', 'B#': 'C', 'C': 'C',
        'C#': 'C#', 'D': 'D', 'Eb': 'Eb', 'E': 'E',
        'F': 'F', 'F#': 'F#', 'G': 'G', 'Ab': 'Ab',
        'A': 'A', 'Bb': 'Bb', 'B': 'B', 'Hb': 'Bb', 'H': 'B'
}
NOTES = ['C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'Ab', 'A', 'Bb', 'B']
NOTES_IN_OCTAVE = len(NOTES)

def get_devices(capture_devices: bool = False) -> Tuple[str, ...]:
    init_by_me = not pygame.mixer.get_init()
    if init_by_me:
        pygame.mixer.init()
    devices = tuple(sdl2_audio.get_audio_device_names(capture_devices))
    if init_by_me:
        pygame.mixer.quit()
    return devices


def play(file_path: str, device: Optional[str] = None):
    if device is None:
        devices = get_devices()
        if not devices:
            raise RuntimeError("No device!")
        device = devices[0]
    print(f"Play: {file_path}", "Device: {device}", device="\n")
    pygame.mixer.init(devicename=device)
    pygame.mixer.music.load(file_path)
    pygame.mixer.music.play()
    try:
        while True:
            sleep(0.1)
    except KeyboardInterrupt:
        pass
    pygame.mixer.quit()



def play_chord(midi_out, notes, vol=100, length=0.5):
    for n in notes:
        midi_out.note_on(n, vol)
    sleep(length)
    for n in notes:
        midi_out.note_off(n, vol)


def exit(outputs):
    for o in outputs:
        del o
    pygame.midi.quit()
    pygame.quit()
    sys.exit()


def note_to_number(note: str, octave: int) -> int:
    assert note in LOOKUP, f'Unknown note: {note}'
    note = LOOKUP[note]
    assert octave > 0 and octave < 11, f'Octave out or range: {octave}'

    note = NOTES.index(note)
    note += (NOTES_IN_OCTAVE * octave)
    assert 0 <= note <= 127, f'Note out of range {note}'
    return note

#                init
# =======================================

def main(midi_out):
# =======================================
    octave = 7
    bank_instrument = {
            0: [4, 19, 57],
            1: [8, 14],
            4: [7, 16, 27, 43],
            23: [4, 64, 94]
            }

    octave = 3
    bank_instrument = {
            5: [7, 15, 31, 65, 75, ],
            18: [26, 27, 52, ],
            13: [7, 17, 37, ]
            }
    """
    bank, instrument, octave
      0, 4, 7
      0, 19, 7
      0, 57, 7
      1, 8, 7
      1, 14, 7
      4, 7, 7
      4, 16, 7
      4, 27, 7
      4, 43, 7 - kocky :D

    """

    length = 0.15

    for instrument in range(127):
        midi_out.set_instrument(instrument)
        print(instrument)
        # for octave in (np.arange(4) + 4)[::-1]:
        for note in ["C", "D", "E", "F", "G", "A", "B"][::-1]:
            ix = note_to_number(note, octave)
            play_chord(midi_out, [ix], length=length)
            sleep(length/3)

    exit([midi_out])



if __name__ == "__main__":
    DEVICE = "Erik WH-1000XM3"
    pygame.init()
    pygame.midi.init()
    port = pygame.midi.get_default_output_id()
    midi_out = pygame.midi.Output(port, 0)
    try:
        main(midi_out)
    except KeyboardInterrupt:
        exit([midi_out])
