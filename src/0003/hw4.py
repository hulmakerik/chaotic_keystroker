import pygame
import random
import time

# --- constants --- (UPPER_CASE names)

WHITE = (255, 255, 255)
BLACK = (0  ,   0,   0)

DISPLAY_WIDTH = 800
DISPLAY_HEIGHT = 800

FPS = 30

BLOCK_WIDTH = 50
BLOCK_HEIGHT = 50

# --- functions --- (lower_case names)

def run():

    mainloop = True

    speed_x = 0
    speed_y = 0

    while mainloop:

        # --- events ---

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                mainloop = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    mainloop = False
                # - start moving -
                elif event.key == pygame.K_UP:
                    speed_y = -10
                elif event.key == pygame.K_DOWN:
                    speed_y = 10
                elif event.key == pygame.K_LEFT:
                    speed_x = -10
                elif event.key == pygame.K_RIGHT:
                    speed_x = 10
            #elif event.type == pygame.KEYUP:
            #    # - stop moving -
            #    if event.key == pygame.K_UP:
            #        speed_y = 0
            #    elif event.key == pygame.K_DOWN:
            #        speed_y = 0
            #    elif event.key == pygame.K_LEFT:
            #        speed_x = 0
            #    elif event.key == pygame.K_RIGHT:
            #        speed_x = 0

        # --- updates ---

        player_rect.move_ip(speed_x, speed_y)

        # --- draws ---

        screen.blit(background, background_rect)
        screen.blit(player, player_rect)
        pygame.display.flip()

        clock.tick(FPS)

# --- main ---

pygame.init()
pygame.display.set_caption('Test Game')

screen = pygame.display.set_mode( (DISPLAY_WIDTH, DISPLAY_HEIGHT) )
screen_rect = screen.get_rect()

background_rect = screen_rect
background = pygame.Surface(background_rect.size)
background.fill(WHITE)
background = background.convert()

player_rect = pygame.Rect(400, 400, BLOCK_WIDTH, BLOCK_HEIGHT)
player = pygame.Surface(player_rect.size)
player.fill(BLACK)

clock = pygame.time.Clock()

run()

pygame.quit()
