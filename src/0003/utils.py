from typing import Callable


class Vectorizer:
    def __init__(self, *items):
        self._items = items

    def __call__(self, *args, **kwargs):
        return Vectorizer(*[item(*args, **kwargs) for item in self._items])

    def __add__(self, other: "Vectorizer"):
        return Vectorizer(*self._items, *other._items)

    def __getattr__(self, *args, **kwargs):
        return Vectorizer(*[getattr(item, *args, **kwargs) for item in self._items])

    def __iter__(self):
        return iter(self._items)

    def __len__(self):
        return len(self._items)

    def __getitem__(self, key):
        return self._items[key]

    def function(self, function: Callable):
        """ call function on each item """
        return Vectorizer(*[function(item) for item in self._items])

    @property
    def items(self):
        return self._items
