import numpy as np
import cv2
import pygame, sys
from pygame.locals import *
from utils import Vectorizer


#Set up the colors
BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0,255,0)
BLUE =  (0,0,255)
WHITE = (255,255,255)


W, H = 1920, 1080
pygame.init()
pygame.display.set_caption('lol')

screen = pygame.display.set_mode((W, H))
screen.fill(BLACK)
screen_rect = screen.get_rect()


background_rect = screen_rect
background = pygame.Surface(background_rect.size)
background.fill(WHITE)
background = background.convert()


player_rect = pygame.Rect(W, H, 10, 10)
player = pygame.Surface(player_rect.size)
player.fill(BLACK)

FPS = 30
clock = pygame.time.Clock()


#Draw the white background onto the surface

#Draw a red circle onto the surface
n = 10
r = 2
XY = np.random.randint((0, 0), (W, H), size=(n, 2))
xy = np.random.randint((0, 0), (W, H))

# c = pygame.draw.circle(windowSurface, WHITE, xy, r)

#Get a pixel array of the surface
# pixArray = pygame.PixelArray(windowSurface)
# pixArray[480][380] = BLACK
# del pixArray

# #Draw the text onto the surface
# windowSurface.blit(text,textRect)

#Draw the window onto the screen
                       # pygame.display.update()

running = True
while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False
    xy += np.random.randint((-1, -1), (2, 2)) # , size=(n, 2))
    xy = np.clip((0, 0), (W, H), xy)
    player_rect.move(*xy)

    screen.blit(background, background_rect)
    screen.blit(player, player_rect)
    pygame.display.flip()
    clock.tick(FPS)

    # pygame.display.update()

pygame.quit()
sys.exit(0)
