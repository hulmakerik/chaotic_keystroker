import cv2
import numpy as np
import matplotlib.pyplot as plt
import vlc
from time import sleep
import sys
from tqdm import tqdm
from colour import Color
from pathlib import Path
import scipy.stats as st


# in our case, we call period half cycle of the wave, becouse there is the sound line
def period_progress(t, freq):
    delta = t * freq / np.pi
    y = delta + np.floor(delta)
    return y - np.floor(y)



def x_coord(t, freq, amp):
    return np.sin(t * freq) * amp


W, H = 3840, 2160
# W, H = 1920, 1080
pad_H = H//10
pad_W = W//2
N = 27
FPS = 60
TD = 1/FPS
MAX_FRAMES = FPS * 500
OUT_FILENAME = Path("long")
GRAY = (130, 130, 130)


amplitude = np.ones(N) * (W - pad_W)/2
frequency = np.linspace(0.5, 0.7, N)
period = np.pi / frequency


base_color = list(Color("darkred").range_to(Color("coral"), N))
base_color += list(Color("coral").range_to(Color("cyan"), N))
base_color += list(Color("cyan").range_to(Color("darkblue"), N))
base_color = base_color[::3]

# base_color = list(Color("indigo").range_to(Color("cyan"), N))
# base_color += list(base_color[-1].range_to(Color("darkgreen"), N))
# base_color = base_color[::2]

# base_color += list(Color("lightcoral").range_to(Color("cyan"), N))
# base_color += list(Color("cyan").range_to(Color("indigo"), N))
# base_color = base_color[::3]

color_pallete = np.array([[c.rgb for c in bc.range_to(Color("white"), 100)] for bc in base_color])
color_pallete = color_pallete[:, :, [2, 1, 0]]  # RGB -> BGR


Y = np.linspace(pad_H, H-pad_H, N)
t = 0


size = W//140
thickness = W//500

# vypnu kdyz je to nula
STRING_SUBDIVISION = 20
GAUSSIAN = True

bg = np.zeros((H, W, 3), dtype=np.uint8)


if STRING_SUBDIVISION == 0:
    bg = cv2.line(bg, (W//2, pad_H//2), (W//2, H-pad_H//2), GRAY, thickness)


render = True
if render:
    fn = str(OUT_FILENAME.with_suffix(".avi"))
    out = cv2.VideoWriter(fn, cv2.VideoWriter_fourcc(*'DIVX'), FPS, (W, H))
    for f in tqdm(range(MAX_FRAMES), total=MAX_FRAMES):
        X = x_coord(t, frequency, amplitude) + W//2
        progress = period_progress(t, frequency)

        img = bg.copy()
        for i in range(N):
            # spagat
            img = cv2.line(img, (W//2, -H//2), (int(X[i]), int(Y[i])), GRAY, max(thickness//6, 2))
        if STRING_SUBDIVISION > 0:
            im = bg.copy()
            if GAUSSIAN:
                assert STRING_SUBDIVISION % 2 == 0
                r = size * 2 + thickness + 1
                bins = np.linspace(-2*r, 2*r, STRING_SUBDIVISION)
                values = st.norm.pdf(bins, 0, r)
                values = (values / np.max(values) * 255).astype(np.uint8)
                for j in range(STRING_SUBDIVISION//2):
                    for i in range(N):
                        x, y, p = int(X[i]), int(Y[i]), progress[i]
                        c = values[j] * (1-p)**2
                        b = bins[j]
                        im = cv2.line(im, (W//2, int(y-b)), (W//2, int(y+b)), (c, c, c), max(thickness, 1))
            else:
                for j in reversed(range(STRING_SUBDIVISION)):
                    k = (j+1)/STRING_SUBDIVISION
                    r = size * 2 + thickness + 1
                    for i in range(N):
                        # blikajici struna
                        x, y, p = int(X[i]), int(Y[i]), progress[i]
                        c = int( (1-k) * (1-p)**2 * 255)
                        im = cv2.line(im, (W//2, int(y-r*k)), (W//2, int(y+r*k)), (c, c, c), max(thickness, 1))
            img = np.maximum(img, im)
        for i in range(N):
            x, y, p = int(X[i]), int(Y[i]), progress[i]
            ix = int(np.clip((1-p)**6, 0, 1) * 99)
            c = color_pallete[i][ix]
            c = (np.array(c) * 255).astype(np.uint8)
            img = cv2.circle(img, (x, y), size, GRAY, thickness)
            img = cv2.circle(img, (x, y), size, c.tolist(), -1)
            # print message
            # font = cv2.FONT_HERSHEY_SIMPLEX
            # image = cv2.putText(img, f"{p:.4f}", (10, y), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        t += TD
        out.write(img)

    out.release()

import mido
from mido import MidiFile, Message, MidiTrack
from music import get_mid, note_to_number, mid2wav
PROGRAM = 8 # 26, 8 (nice)
FIRST_OCTAVE = 2
DURATION = 0.2      # seconds (0.2)

mid = get_mid([PROGRAM] * N)
tempo = 500000  # microseconds per beat
tpb = mid.ticks_per_beat
duration = int(mido.second2tick(DURATION, tpb, tempo))
gap = np.array([mido.second2tick(p, tpb, tempo) for p in period])


keys = ["C", "D", "E", "F", "G", "A", "B"]
total_time = MAX_FRAMES * TD
repetitions = total_time / period
total_time_tick = mido.second2tick(total_time, tpb, tempo)


for i, track in enumerate(mid.tracks):
    note = note_to_number(keys[i%len(keys)], i//len(keys) + FIRST_OCTAVE)

    # corect_times = np.arange(repetitions[i]+1, dtype=np.double) * gap[i]
    # tick_times = np.round(corect_times).astype(np.int64)

    corect_times = np.arange(repetitions[i], dtype=np.double) * period[i]
    tick_times = np.round([mido.second2tick(p, tpb, tempo) for p in corect_times]).astype(np.int64)
    deltas = np.diff(tick_times, prepend=0)

    track.append(Message('note_on', note=note, channel=0, velocity=127, time=0))
    track.append(Message('note_off', note=note, channel=0, velocity=127, time=duration))
    """
    for j in range(1, len(tick_times)):
        if tick_times[j] > total_time_tick:
            break
        s = tick_times[j] - (tick_times[j-1] + duration)
        track.append(Message('note_on', note=note, channel=0, velocity=127, time=s))

        e = duration
        if tick_times[j] + e > total_time_tick:
            e = int(total_time_tick - tick_times[j])
        track.append(Message('note_off', note=note, channel=0, velocity=127, time=e))
    """
    for j, d in enumerate(deltas[1:]):
        if tick_times[j+1] > total_time_tick:
            break
        track.append(Message('note_on', note=note, channel=0, velocity=127, time=d-duration))
        if tick_times[j+1] + duration > total_time_tick:
            end_time = int(total_time_tick - tick_times[j+1])
            track.append(Message('note_off', note=note, channel=0, velocity=127, time=end_time))
            break
        track.append(Message('note_off', note=note, channel=0, velocity=127, time=duration))

export = True
if export:
    mid2wav(mid, OUT_FILENAME, sf="sf/SGM-v2.01-HQ-v3.0.sf2", wait=True)

    avi_pth = str(OUT_FILENAME.with_suffix(".avi"))
    wav_pth = str(OUT_FILENAME.with_suffix(".wav"))
    out_pth = str(OUT_FILENAME.with_suffix(".mp4"))
    # which way do they spin?
    command = f"yes | ffmpeg -i {avi_pth} -i {wav_pth} -c:v copy -c:a aac {out_pth}"
    import subprocess
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()
    print("done")