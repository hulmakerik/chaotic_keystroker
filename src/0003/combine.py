import moviepy.editor as mpy
from moviepy.editor import VideoFileClip, AudioFileClip, CompositeAudioClip

from animation import OUT_FILENAME, FPS
from pathlib import Path


clip = VideoFileClip(str(OUT_FILENAME.with_suffix('.avi')))
audio = mpy.AudioFileClip(str(OUT_FILENAME.with_suffix('.wav')))
audio = audio.subclip(0, clip.duration)
clip = clip.set_audio(audio)
# new_audioclip = CompositeAudioClip([audio])
# clip.audio = new_audioclip
# clip.write_videofile(str(OUT_FILENAME.with_suffix('.mp4')), codec='libx264', fps=FPS)
out = Path(str(OUT_FILENAME.stem) + "_out").with_suffix('.avi')
clip.write_videofile(str(out), codec='png', fps=FPS)
