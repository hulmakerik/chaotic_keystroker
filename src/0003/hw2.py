import pyglet
from time import time
from pyglet import shapes
import numpy as np


W, H = 1920, 1080
window = pyglet.window.Window(width=W, height=H)


border_H = H/10
border_W = W/10



n = 100
# amplitude = np.arange(n) / n
XY = np.random.randint((0, 0), (W, H), size=(n, 2))

radius = 5
t0 = time()


batch = pyglet.graphics.Batch()
circles = [shapes.Circle(x, y, radius, color=(255, 225, 255), batch=batch) for x, y in XY]

# image_data = image.get_image_data(W, H)
# width = image_data.width
# data = image_data.get_data('RGB', 3*width)
# data = image_data.get_data('RGB', 3*width)
# print(data)
# image = pyglet.image.AbstractImage(W, H)
# image_data = image.get_image_data()
# data = image_data.get_data('RGB', 3*width)

from pyglet.gl.gl import GLubyte
data = np.zeros((H, W, 3), dtype=np.uint8)
data[300:350, 300:350, :] = 255

asdf = GLubyte * data.size
tex_data = asdf(*data.flatten())
image = pyglet.image.ImageData(
    data.shape[0],
    data.shape[1],
    "RGBA",
    tex_data,
    pitch = data.shape[1] * data.shape[2] * 1
    )

# image.blit(0, 0)
sprite = pyglet.sprite.Sprite(image, x = 0, y = 0)


def update_batch():
    global circles, XY, image
    step_xy = np.random.randint((-1, -1), (2, 2), size=(n, 2))
    XY += step_xy
    XY = np.clip((0, 0), (W, H), XY)
    for xy, c in zip(XY, circles):
        c.position = xy
    data = image.get_image_data()
    format = 'RGBA'
    pitch = data.width * len(format)
    pixels = data.get_data(format, pitch)
    print(len(pixels))


@window.event
def on_draw():
    global image, t0, batch, window, sprite
    window.clear()
    update_batch()
    new_batch = pyglet.graphics.Batch()
    # for x, y in zip(X, Y):
    #     label = pyglet.text.Label(f"{x:.5f}", font_name='Times New Roman',
    #                       font_size=10,
    #                       x=window.width//2, y=y,
    #                       batch = new_batch,
    #                       anchor_x='center', anchor_y='center')
    #     label.draw()
    batch.draw()
    sprite.draw()


flag = False

@window.event
def on_key_press(symbol, modifiers):
    global circles
    circles[0].position = circles[0].x + 10, circles[0].y
    # global flag, image, im1, im2
    # image = im1 if flag else im2
    # flag = not flag
    #     image1.blit(0, 0)
    # else:
    #     image2.blit(0, 0)
    # empty = not empty
    print('A key was pressed')


pyglet.app.run()
