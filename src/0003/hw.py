import pyglet
from time import time
from pyglet import shapes
import numpy as np


W, H = 1920, 1080
window = pyglet.window.Window(width=W, height=H)


border_H = H/10
border_W = W/10



n = 100
# amplitude = np.arange(n) / n
amplitude = np.ones(n) * 500
frequency = np.linspace(0.5, 10, n)
Y = np.linspace(border_H, H-border_H, n)
X = np.zeros(n)
size = 5
t0 = time()


batch = pyglet.graphics.Batch()
circles = [shapes.Circle(x, y, size, color=(255, 225, 255), batch=batch) for x, y in zip(X, Y)]


def update_batch(t):
    global circles, X, Y, frequency, amplitude, W
    X = np.sin(t * frequency) * amplitude
    for x, c in zip(X, circles):
        c.x = x + W/2


@window.event
def on_draw():
    global image, t0, batch, window
    window.clear()
    update_batch((time()-t0)/2)
    new_batch = pyglet.graphics.Batch()
    # for x, y in zip(X, Y):
    #     label = pyglet.text.Label(f"{x:.5f}", font_name='Times New Roman',
    #                       font_size=10,
    #                       x=window.width//2, y=y,
    #                       batch = new_batch,
    #                       anchor_x='center', anchor_y='center')
    #     label.draw()
    batch.draw()


flag = False

@window.event
def on_key_press(symbol, modifiers):
    global circles
    circles[0].position = circles[0].x + 10, circles[0].y
    # global flag, image, im1, im2
    # image = im1 if flag else im2
    # flag = not flag
    #     image1.blit(0, 0)
    # else:
    #     image2.blit(0, 0)
    # empty = not empty
    print('A key was pressed')


pyglet.app.run()
