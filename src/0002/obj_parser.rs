use std::env;
use std::fs::File;
use std::io::{ self, BufRead, BufReader };
use std::path::PathBuf;

// https://github.com/marczych/RayTracer
// https://pellacini.di.uniroma1.it/teaching/graphics06/lectures/03_RayTracing_Web.pdf
// https://en.wikipedia.org/wiki/Phong_reflection_model
// https://courses.fit.cvut.cz/BI-PGA/@B211/lectures/index.html
// https://en.wikipedia.org/wiki/Wavefront_.obj_file

fn read_lines(filepath: &PathBuf) -> io::Lines<BufReader<File>> {
    // Open the file in read-only mode.
    let f = File::open(filepath);
    let f = match f {
        Ok(file) => file,
        Err(e) => panic!("Problem openint the filepath: {:?}. Error: {:?}", filepath, e),
    };

    // Read the file line by line, and return an iterator of the lines of the file.
    return io::BufReader::new(f).lines();
}


fn parse_values(iter: &mut std::str::SplitWhitespace) -> Vec<f32> {
    let mut vec = Vec::new();
    while let Some(token) = iter.next() {
        vec.push(token.parse::<f32>().unwrap());
    }
    return vec;
}


pub struct Material {
    name: String,
    ka: Vec<f32>,
    kd: Vec<f32>,
    ks: Vec<f32>,
    ns: f32,
    ni: f32,
    d: f32,
    illum: i32,
}

impl Material {
        pub fn new() -> Self {
            Self { name: String::new(), ka: Vec::new(), kd: Vec::new(), ks: Vec::new(), ns: 0.0, ni: 0.0, d: 0.0, illum: 0 }
        }
    
    
        pub fn parse_line(&mut self, line: &String) {
            let mut iter = line.split_whitespace();
            let identifier = iter.next().unwrap();
    
            match identifier {
                "newmtl" => { self.name = iter.next().unwrap().to_string(); }
                "Ka" => { self.ka = parse_values(&mut iter); }
                "Kd" => { self.kd = parse_values(&mut iter); }
                "Ks" => { self.ks = parse_values(&mut iter); }
                "Ns" => { self.ns = iter.next().unwrap().parse::<f32>().unwrap(); }
                "Ni" => { self.ni = iter.next().unwrap().parse::<f32>().unwrap(); }
                "d" => { self.d = iter.next().unwrap().parse::<f32>().unwrap(); }
                "illum" => { self.illum = iter.next().unwrap().parse::<i32>().unwrap(); }
                _ => {}
            }
        }
}


fn parse_materials(filepath: &PathBuf) -> Vec<Material> {
    let mut materials: Vec<Material> = Vec::new();
    materials.push(Material::new());

    for line in read_lines(&filepath) {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }
        let identifier = line.split_whitespace().next().unwrap();
        if identifier == "newmtl" {
            if !materials.last_mut().unwrap().name.is_empty() {
                materials.push(Material::new());
            }
        }
        materials.last_mut().unwrap().parse_line(&line);
    }

    return materials;
}


pub struct  Object {
    name: String,
    material: String,
    v: Vec<Vec<f32>>,           // (N , 3)
    vn: Vec<Vec<f32>>,          // (N , 3)
    vt: Vec<Vec<f32>>,          // (N , 2)
    f: Vec<Vec<Vec<i32>>>,     // (M, ?, 3)
}


// fn print_type_of<T>(_: &T) {
//     println!("{}", std::any::type_name::<T>())
// }


impl Object {

    pub fn new() -> Self {
        Self { name: String::new(), material: String::new(), v: Vec::new(), vn: Vec::new(), vt: Vec::new(), f: Vec::new()}
    }

    // fn parse_values(iter: &mut std::str::SplitWhitespace) -> Vec<f32> {
    //     let mut vec = Vec::new();
    //     while let Some(token) = iter.next() {
    //         vec.push(token.parse::<f32>().unwrap());
    //     }
    //     return vec;
    // }

    pub fn parse_line(&mut self, line: &String, offset: i32) {
        let mut iter = line.split_whitespace();
        let identifier = iter.next().unwrap();

        match identifier {
            "o" => { self.name = iter.next().unwrap().to_string(); }
            "usemtl" => { self.material = iter.next().unwrap().to_string(); }
            "v" => { self.v.push(parse_values(&mut iter)); }
            "vn" => { self.vn.push(parse_values(&mut iter)); }
            "vt" => { self.vt.push(parse_values(&mut iter)); }
            "f" => {
                let mut face = Vec::new();
                while let Some(token) = iter.next() {
                    let mut face_token = Vec::new();
                    for token in token.split("/") {
                        face_token.push(token.parse::<i32>().unwrap() - offset);
                    }
                    face.push(face_token);
                }
                self.f.push(face);
            }
            _ => {}
        }
    }
}


fn parse_objects(filepath: &PathBuf) -> Vec<Object> {
    let mut objects: Vec<Object> = Vec::new();
    // let mut materials = parse_materials(&PathBuf::from("sphere_spiral.mtl"));
    objects.push(Object::new());
    let mut offset = 1;

    for line in read_lines(&filepath) {
        let line = line.unwrap();
        if line.is_empty() {
            continue;
        }
        let identifier = line.split_whitespace().next().unwrap();
        if identifier == "o" {
            if !objects.last_mut().unwrap().name.is_empty() {
                // objects.push(Object { name: String::new(), v: Vec::new(), vn: Vec::new(), vt: Vec::new()});
                offset += objects.last_mut().unwrap().v.len() as i32;
                objects.push(Object::new());
            }
        }
        objects.last_mut().unwrap().parse_line(&line, offset);
    }
    return objects;
}



fn main() {
    // Stores the iterator of lines of the file in lines variable.
    let args: Vec<String> = env::args().collect();
    let mut filepath = PathBuf::from("tetxtured.obj");
    if args.len() > 1 {
        filepath = PathBuf::from(&args[1]);
    }

    let objects = parse_objects(&filepath);

    let mtl_filepath = filepath.with_extension("mtl");
    let materails = parse_materials(&mtl_filepath);
    println!("{:?}", mtl_filepath);

    for m in materails {
        println!("--------------------------");
        println!("{:?}", m.name);
        println!("{:?}", m.kd);
    }

    for obj in objects {
        println!("--------------------------");
        println!("{:?}", obj.name);
    }
}