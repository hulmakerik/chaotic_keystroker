import numpy as np
import cv2
from pathlib import Path
from typing import Union, List
from dataclasses import dataclass, field, asdict
from collections import defaultdict
from pprint import pprint


"""
syntax: 
    <indentifier> <data>

    <identifier>: o             # named object group object name
        <data>: [object name]
    
    <identifier>: g             # group name
        <data>: [group name]    # polygon groups
    
    <identifier>: v             # geometric vertices (w is optional and defaults to 1.0.)
        <data>: x y z [w]
    
    <identifier>: vt            # texture coordinates (between 0 and 1. v, w are optional and default to 0.)
        <data>: u [v, w]
    
    <identifier>: vn            # vertex normals (might not be unit vectors)
        <data>: x y z
    
    <identifier>: vp            # parameter space vertices in (u, [v, w]) form; free form geometry statement
        <data>: u [v, w]
    
    <identifier>: f             # polygonal face element
        <data>: v1 v2 v3 ...                            # Vertex indices
        <data>: v1/vt1 v2/vt2 v3/vt3 ...                # Vertex and texture indices
        <data>: v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3 ...    # Vertex, texture and normal indices
    
    <identifier>: l             # line element - specify the order of the vertices which build a polyline
        <data>: v1 v2 v3 ...
    
    <identifier>: #             # comment
        <data>: comment_string
    
    <indentifier>: mtllib       # material library
        <data>: [external .mtl file name]
    
    <identifier>: usemtl        # material name
        <data>: [material_name]
    
    <identifier>: s             # smooth shading across polygons is enabled by smoothing groups
        <data>: group_number


"""

@dataclass
class Object:
    """
    in the .obj file, there can be multiple objects
    each object starts wih "o"
    """
    o: str = field(default_factory=str)
    v: list = field(default_factory=list)
    vt: list = field(default_factory=list)
    vn: list = field(default_factory=list)
    vp: list = field(default_factory=list)
    f: list = field(default_factory=list)
    l: list = field(default_factory=list)
    mtl:  str = field(default_factory=str)

    def bake(self):
        self.v = np.array(self.v, dtype=np.float32)
        self.vt = np.array(self.vt, dtype=np.float32)
        self.vn = np.array(self.vn, dtype=np.float32)
        self.vp = np.array(self.vp, dtype=np.float32)
        # self.f = np.array(self.f, dtype=np.int32)     # possibly of different length
        # self.l = np.array(self.l, dtype=np.int32)     # possibly of different length
    
    def __getitem__(self, key: str):
        return getattr(self, key, list())


class ObjParser:
    def __init__(self, path: Union[str, Path]):
        self.objects = []
        self.queue = None
        with Path(path).open("r") as f:
            self.queue = f.readlines()
        
        self._ignore = set(["g", "s", "#"])
        self.mtllib_filename = None
    
    @property
    def _obj(self):
        """ current object """
        return self.objects[-1] if self.objects else None
    
    def o(self, tokens: list):
        assert len(tokens) == 1
        if self._obj:
            self._obj.bake()
        self.objects.append(Object(tokens[0]))
    
    def usemtl(self, tokens: list):
        assert len(tokens) == 1
        assert self._obj, "usemtl must be after o"
        self._obj.mtl = tokens[0]
    
    def mtllib(self, tokens: list):
        assert len(tokens) == 1
        self.mtllib_filename = tokens[0]
 
    def v(self, tokens: list):
        if len(tokens) == 3:
            return self.v(tokens + [1.0])
        assert len(tokens) == 4
        data = [float(x) for x in tokens]
        self._obj['v'].append(data)

    def vt(self, tokens: list):
        assert len(tokens) >= 1
        if len(tokens) < 3:
            return self.vt(tokens + [0.0])
        assert len(tokens) == 3
        data = [float(x) for x in tokens]
        self._obj['vt'].append(data)
    
    def vn(self, tokens: list):
        assert len(tokens) == 3
        data = [float(x) for x in tokens]
        self._obj['vn'].append(data)
    
    def f(self, tokens: list):
        # TODO: I didn't check if this is valid: v1 v2/vt2 v3//vn3
        # TODO: Easily checkable using numpy
        assert len(tokens) >= 3
        data = []
        for token in tokens:
            indices = token.split("/")
            assert len(indices) == 3, "Blender exported always 3 indices v/vt/vn"
            data.append([int(indices[0]), int(indices[1]), int(indices[1])])
        self._obj['f'].append(data)
    
    def l(self, tokens: list):
        assert len(tokens) >= 2
        data = [int(x) for x in tokens]
        self._obj['l'].append(data)
    
    def _error(self, identifier: str):
        def inner(*args, **kwargs):
            raise NotImplementedError(f"Unknown token {identifier}.")
        return inner

    def parse(self):
        assert self.queue, "No data to parse."
        while self.queue:
            line = self.queue.pop(0).strip()
            identifier, *tokens = line.split()
            if identifier not in self._ignore:
                procedure = getattr(self, identifier, self._error(identifier))
                procedure(tokens)
        self._obj.bake()
        return self.objects


def plot_wireframe(ax, o: Object, c: str, offset):
        ax.scatter(*o.v[:, :-1].T, c=c)
        for face in o.f:
            indices = np.array(face + [face[0]], dtype=np.int32)[:, 0] - offset
            vertices = o.v[indices, :-1]
            ax.plot(*vertices.T, c=c)


def parse_mtl(path: str):
    queue = None
    with Path(path).open("r") as f:
        queue = f.readlines()
    assert queue, "empty queue"

    materials = defaultdict(dict)

    name = None
    while queue:
        line = queue.pop(0).strip()
        if line:
            identifier, *tokens = line.split()
            if identifier == "newmtl":
                name = tokens[0]
            elif identifier == "#":
                continue
            else:
                materials[name][identifier] = np.array(tokens, dtype=np.float32)
    return materials


def main():
    # p = Parser("0002/dating.obj")
    p = ObjParser("tetxtured.obj")
    # p = Parser("0002/cube.obj")
    # p = Parser("0001/cursed-bread/cursed_bread.obj")
    objects = p.parse()
    color = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    offset = 1

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    for i, o in enumerate(objects):
        c = color[i % len(color)]
        plot_wireframe(ax, o, c, offset)
        offset += len(o.v)
    plt.show()
    materials = parse_mtl(p.mtllib_filename)
    pprint(materials)


if __name__ == "__main__":
    main()