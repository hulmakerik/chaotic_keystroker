import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt


SIZE, THICKNESS = 13, 3
n = 20
r = SIZE + THICKNESS


x = np.linspace(-3, 3, n)
y = st.norm.pdf(x, loc=0, scale=1)
y = y / y.max()

plt.scatter(x, y)
plt.axis('equal')
plt.show()