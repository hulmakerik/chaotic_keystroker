import numpy as np
import scipy.stats as st
import cv2
import matplotlib.pyplot as plt
from pathlib import Path
from tqdm import tqdm
import mido
from mido import MidiFile, MidiTrack, Message
from typing import List, Tuple
from music import *
import sys
from colour import Color


# constants 
N = 27  # number of balls
FPS = 100  # frames per second
# I chose 100 FPS  bevouse between frames ther is a 0.01s difference
# Other frame rates might cause sync issues due to the floating point precision
# Later we can downsample to 60FPS

TD = 1/FPS  # time delta
MAX_FRAMES = FPS * 1000
W, H = 1920, 1080  # width and height of the video
# W, H = 3840, 2160
pad_H, pad_W = H//10, W//10  # padding
Y = np.linspace(pad_H, H-pad_H, N)  # y coordinates (constant)


AMP = np.ones(N) * (W - pad_W) / 2  # amplitude
FREQ = np.linspace(0.5, 0.7, N)      # frequency
PERIOD = np.pi / FREQ               # half-period - we only care about half the cycle

SIZE = W//140  # size of the ball
THICKNESS = max(W//500, 1)  # thickness of the line
GRAY = (128, 128, 128)  # gray color
base_color = Color("red").range_to(Color("lime"), N)
color_pallete = [[c.rgb for c in bc.range_to(Color("white"), 100)] for bc in base_color]
# convert to RGB -> BGR (opencv uses this standard)
color_pallete = np.array(color_pallete)[:, :, [2, 1, 0]]

STRING_SUBDIVISION = 40
GAUSSIAN = True


# music constants
# program 8, 26 are nice, 32 maybe too
PROGRAM = 8 if len(sys.argv) == 1 else int(sys.argv[1])  # program number
OUTPUT = Path("out/output_sync") if len(sys.argv) <= 2 else Path(sys.argv[2])  # output file
DURATION = 0.2  # duration of the note in seconds
FIRST_OCTAVE = 3  # first octave

# each ball has a note, the notes follow keys
# after the last key, the octave is increased (starting at FIRST_OCTAVE)
NOTE = np.array([note2number(KEYS[i%len(KEYS)], i//len(KEYS) + FIRST_OCTAVE) for i in range(N)])


def get_X(t, freq, amp):
    return np.sin(t * freq) * amp


def get_period_progress(t, period):
    return (t % period) / period


def plot(img):
    plt.figure(figsize=(16, 9))
    plt.axis("off")
    plt.imshow(img)
    plt.show()


def generate_video(t = 0):
    bg = np.zeros((H, W, 3), dtype=np.uint8)    # background

    if STRING_SUBDIVISION == 0:
        bg = cv2.line(bg, (W//2, H-pad_H), (W//2, pad_H), GRAY, THICKNESS)

    fn = str(OUTPUT.with_suffix(".avi"))
    out = cv2.VideoWriter(fn, cv2.VideoWriter_fourcc(*'DIVX'), FPS, (W, H))
    note_times = [[0] * N]

    for f in tqdm(range(MAX_FRAMES), total=MAX_FRAMES):
        img = bg.copy()
        X = get_X(t, FREQ, AMP) + W//2  # x coordinates, shift to the center
        progress = get_period_progress(t, PERIOD)
        # draw the strings
        for i in range(N):
            x, y = int(X[i]), int(Y[i])
            img = cv2.line(img, (x, y), (W//2, -H//2), GRAY, max(2, THICKNESS//6))
        
        # draw gaussians on the center line
        im = bg.copy()
        cnt = STRING_SUBDIVISION + (STRING_SUBDIVISION % 2)
        r = (SIZE + THICKNESS)*2 + 1
        bins = np.linspace(-r, r, cnt)
        values = st.norm.pdf(np.linspace(-3, 3, cnt), loc=0, scale=1)
        values = values / np.max(values)
        lines = []
        for j in range(cnt//2):
            for i in range(N):
                x, y, p = int(X[i]), int(Y[i]), progress[i]
                c = int(values[j] * (1-p)**2 * 255)
                b = bins[j]
                if c > 0:
                    lines.append((c, (W//2, int(y-b)), (W//2, int(y+b))))
        # It's much more efficient to sort them and start from the darkest than to do np.maximum over pixels every time
        for c, p0, p1 in sorted(lines):
            im = cv2.line(im, p0, p1, (c, c, c), THICKNESS)
        img = np.maximum(im, img)  # not to disturb the strings
        
        # draw the balls
        for i in range(N):
            x, y, p = int(X[i]), int(Y[i]), progress[i]
            img = cv2.circle(img, (x, y), SIZE, GRAY, THICKNESS)  # white outline
            cix = int(np.clip((1-p)**6, 0, 1) * 99)
            c = (np.array(color_pallete[i][cix]) * 255).astype(np.uint8)
            img = cv2.circle(img, (x, y), SIZE, c.tolist(), -1)  # fill (color)
        out.write(img)
        t += TD
    out.release()


def generate_audio():
    mid = get_mid([PROGRAM] * N)
    
    # constants
    tempo = 500000
    tpb = mid.ticks_per_beat

    # find where the balls cross the center line
    total_time = MAX_FRAMES * TD
    repetitions = total_time / PERIOD
    duration = int(mido.second2tick(DURATION, tpb, tempo))
    total_time_tick = mido.second2tick(MAX_FRAMES * TD, tpb, tempo)

    for i, track in enumerate(mid.tracks):
        note_times = np.arange(repetitions[i]) * PERIOD[i]
        deltas = np.diff(note_times, prepend=0)
        # at the begining there is a note
        track.append(Message('note_on', time=0, note=NOTE[i], channel=0, velocity=127))
        track.append(Message('note_off', time=duration, note=NOTE[i], channel=0, velocity=127))

        for j, d in enumerate(deltas[1:]):
            tick_time = mido.second2tick(note_times[j+1], tpb, tempo)
            s = int(mido.second2tick(d, tpb, tempo) - duration)
            if tick_time < total_time_tick:
                track.append(Message('note_on', time=s, note=NOTE[i], channel=0, velocity=127))
                # the audio file might be a little bit longer than the video.
                # later we can fix it
                track.append(Message('note_off', time=duration, note=NOTE[i], channel=0, velocity=127))
    return mid


def generate_audio2(t=0):
    """
    Instead of computing times of the notes, we can synchronize them with the video.
    """
    mid = get_mid([PROGRAM] * N)

    # constants
    tempo = 500000
    tpb = mid.ticks_per_beat
    atol = np.finfo(float).eps

    # find where the balls cross the center line (inefficient, but simple and reduces the error)
    last_state = np.ones(N)
    bell_frame = np.zeros((MAX_FRAMES, N), dtype=bool)
    # for f in tqdm(range(MAX_FRAMES), total=MAX_FRAMES):
    for f in range(MAX_FRAMES):
        state = get_period_progress(t, PERIOD)
        # True, if the ball crossed the center line
        bell_frame[f] = state < last_state + atol
        t += TD
        last_state = state
    
    duration = int(mido.second2tick(DURATION, tpb, tempo))
    for i, (frames_mask, track) in enumerate(zip(bell_frame.T, mid.tracks)):
        frames = np.where(frames_mask)[0]
        times = frames * TD     # subtracting floats is not precise, let's use integers
        ticks = np.rint([mido.second2tick(t, tpb, tempo) for t in times]).astype(int)
        diffs = np.diff(ticks, prepend=0)
        note = NOTE[i]

        track.append(Message('note_on', time=0, note=note, channel=0, velocity=127))
        track.append(Message('note_off', time=duration, note=note, channel=0, velocity=127))

        for d in diffs[1:]:
            track.append(Message('note_on', time=d-duration, note=note, channel=0, velocity=127))
            # the audio file might be a little bit longer than the video.
            # later we can fix it
            track.append(Message('note_off', time=duration, note=note, channel=0, velocity=127))
    return mid


def main():
    t0 = 0
    generate_video(t=t0)
    mid = generate_audio2(t=t0)
    mid_out = Path(OUTPUT).with_suffix(".mid")
    mid.save(mid_out)
    sf = "sf/SGM-v2.01-HQ-v3.0.sf2"
    mid2wav(mid_out, sf)
    combine(mid_out)

if __name__ == "__main__":
    main()