from typing import List
import subprocess
from pathlib import Path
from mido import MidiFile, MidiTrack, Message


NOTES = ["C", "C#", "D", "Eb", "E", "F", "F#", "G", "Ab", "A", "Bb", "B"]
NOTES_IN_OCTAVE = len(NOTES)
KEYS = ["C", "D", "E", "F", "G", "A", "B"]


def note2number(note, octave=0):
    """Converts a note to a number. C0 is 0, C1 is 12, C2 is 24, etc."""
    assert octave >= 0 and octave <= 11
    note = NOTES.index(note)
    note += octave * NOTES_IN_OCTAVE
    assert note >= 0 and note <= 127
    return note


def get_mid(programs: List):
    mid = MidiFile(type=1)
    for p in programs:
        track = MidiTrack()
        track.append(Message('program_change', program=p, time=0, channel=0))
        mid.tracks.append(track)
    return mid


def mid2wav(filename, sound_font=None):
    if sound_font is None:
        sound_font = ""

    wav_out = str(Path(filename).with_suffix(".wav"))
    mid_out = str(Path(filename).with_suffix(".mid"))
    
    command = f"fluidsynth {sound_font} {mid_out} -F {wav_out}"
    # run subprocess popen with commnd
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.wait()
    return process.returncode


def play_mid(filename, sound_font=None, wait=False):
    if sound_font is None:
        sound_font = ""

    mid_out = str(Path(filename).with_suffix(".mid"))
    command = f"fluidsynth {sound_font} {mid_out} -in"
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if wait:
        process.wait()


def combine(filename):
    avi_out = str(Path(filename).with_suffix(".avi"))
    wav_out = str(Path(filename).with_suffix(".wav"))
    out = str(Path(filename).with_suffix(".mp4"))

    command = f"yes | ffmpeg -i {avi_out} -i {wav_out} -c:v copy -c:a aac {out}"
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.wait()
    print("done!")
