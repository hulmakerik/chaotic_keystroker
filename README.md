# chaotic_keystroker

## video ideas

- udelej kombinaci odrazejicich se hrajicich kulicek a tohoto - https://www.youtube.com/watch?v=-VjbYI-eaTQ
- double pendulum visualization - https://www.youtube.com/watch?v=U7SLv0ePWU0, https://www.youtube.com/watch?v=H_trgmAk5kY
- SLAM from scratch, wide baseline stereo from scratch
- keypoint tracking, track moving keyboard under webcam
- frontier based autonomous driving - prepare docker, then do the programming
- satisfying geometry - fractals
- fluid mechanics simulator
- create a game, then apply some reinforcement learning algorithm
- neural network from scratch using only numpy - MNIST
- reinforcement learning using the network from scratch
- rubick's cube solver
- image 3D reconstruction, image panorama
- solving computer programming challanges
- pixel art otacejici se levitujici houbicka z maria, co je nad nejakym cilindrem s vodou, ze ktereho jde zare. Udelej 3d model houbicky
- 3D snake - udelej hada, co se muze pohybovat v krychli. Bude to grid, budou videt trochu linky, tam kde je had to bude solid matter. Pohled se zmeni tak, aby to pokazde vypadalo jako ze je had brichem dolu. Conrols zustanou stejne. Bude se tedy otacet svet.
- game of life
- bipedal walker - Teaching AI how to Walk
- LLVM compiler
- music visualizer - https://learnpython.com/blog/plot-waveform-in-python/
- mandelbrot zoom
- image colloring


## TODO:
- under each video - put study materials, add timestamps, chapters that explain what you do, repository path
- create banners and screens for AFK, preparation, NDA content
- buy camera, microphone, mount them
- create banners, etc
- support - BTC, ETH, account, IDK
- upgrade network connection - use cable, bandwith
- music programming
- intro video
