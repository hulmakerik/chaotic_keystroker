import numpy as np
import cv2


cap = cv2.VideoCapture(0)
n = 20

kernel = np.ones((n, n), np.float32) / n**2


while cap.isOpened():
    ret, frame = cap.read()
    # frame = cv2.filter2D(frame, -1, kernel)
    # frame = cv2.GaussianBlur(frame, (11, 11), 0)
    frame = cv2.fastNlMeansDenoisingColored(frame, None, 11, 6, 7, 21)
    cv2.imshow('webcam', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
